# hugo-theme-whiteglass

Hugo port of jekyll-theme [whiteglass](https://github.com/yous/whiteglass) from Yous.

SASS files are directly copied from jekyll-whiteglass.

## Quick Start

Create a new site with Hugo:

```
hugo new site my-new-site
cd my-new-site
```

Add this repository as a git submodule under `themes`:

`git submodule add https://gitlab.com/rakuyo/hugo-whiteglass.git themes/whiteglass`

Replace config.toml with config.yaml:

```
rm config.toml && cp themes/whiteglass/config.yaml ./
```

Copy `about.md` and  `archive.md` in `theme/whiteglass` to `content` to have the links work in the navigation bar:

`cp themes/whiteglass/about.md themes/whiteglass/archive.md content`

Create a new post:

`hugo new post/hello-world.md`

Start the blog:

`hugo -D serve`

## Customization

### layouts

CSopy the `html` file  you want to modify to site's `layouts` folder, e.g. `footer.html` :

```
mkdir -p layouts/partials
cp themes/whiteglass/layouts/partials/footer.html ./layouts/partials/
```

### CSS

Create a `assets` folder and copy the `main.scss`:

`mkdir assets && cp themes/whiteglass/assets/main.scss ./assets/`

Edit `main.scss` to overwrite default settings.

### i18n

Create `i18n` folder and add translations:

`mkdir i18n && touch cn.yaml`

add a translation for one of the categories:

```
# category "unnamed" will be translated
- id: unnamed
  translation: "未命名"
```
### Disqus

Setup `disqusShortname` in `config.yaml` to use a Disqus comment.

Enabled delay load.

### Staticman

WIP

### Robot.txt

As default, the template under `layout/robots.txt` allows all pages and provides a sitemap entry.

## Todos
- FontSpider for better Chinese font
- RSS Feed
- LaTex syntax support

## License

Hugo-whiteglass is licensed under the MIT license. Check the [LICENSE](LICENSE) file for details.

